﻿using System;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;

[ExecuteInEditMode]
public class MyPlaneGenerator : MonoBehaviour
{
    public bool updateEveryFrame, useLayeredNoise, flattenEdges;
    public int resolution = 64;
    public float height = -2f;
    public Vector2 offsetOverTime = new Vector2(0, .15f), noiseScale = new Vector2(.2f, .2f);
    
    Mesh mesh;
    Vector3[] vertices;
    int[] triangles;
    Vector2[] uvs;

    void CreateShapes()
    {
        vertices = new Vector3[(resolution + 1) * (resolution + 1)];

        for (int i = 0, z = 0; z <= resolution; z++)
        {
            for (int x = 0; x <= resolution; x++)
            {
                float y;
                if (useLayeredNoise && flattenEdges)
                {
                    y = (Mathf.PerlinNoise(x * noiseScale.x + offsetOverTime.x * Time.time, z * noiseScale.y + offsetOverTime.y * Time.time) +
                    Mathf.PerlinNoise(x * noiseScale.x * 5f + offsetOverTime.x * Time.time * .5f, z * noiseScale.y * 5f + offsetOverTime.y * Time.time * .5f) * .2f) *
                    height * Mathf.Sin((float)x / (float)resolution * Mathf.PI) * Mathf.Sin((float)z / (float)resolution * Mathf.PI);
                }
                else if (useLayeredNoise)
                {
                    y = (Mathf.PerlinNoise(x * noiseScale.x + offsetOverTime.x * Time.time, z * noiseScale.y + offsetOverTime.y * Time.time) +
                    Mathf.PerlinNoise(x * noiseScale.x * 5f + offsetOverTime.x * Time.time * .5f, z * noiseScale.y * 5f + offsetOverTime.y * Time.time * .5f) * .2f) *
                    height;
                }
                else if (flattenEdges)
                {
                    y = Mathf.PerlinNoise(x * noiseScale.x + offsetOverTime.x * Time.time, z * noiseScale.y + offsetOverTime.y * Time.time) *
                    height * Mathf.Sin((float)x / (float)resolution * Mathf.PI) * Mathf.Sin((float)z / (float)resolution * Mathf.PI);
                }
                else
                {
                    y = Mathf.PerlinNoise(x * noiseScale.x + offsetOverTime.x * Time.time, z * noiseScale.y + offsetOverTime.y * Time.time) *
                    height;
                }
                vertices[i] = new Vector3(x, y, z);
                i++;
            }
        }

        int vert = 0, tris = 0;
        triangles = new int[resolution * resolution * 6];

        for (int z = 0; z < resolution; z++)
        {
            for (int x = 0; x < resolution; x++)
            {
                triangles[tris + 0] = vert + 0;
                triangles[tris + 1] = vert + resolution + 1;
                triangles[tris + 2] = vert + 1;
                triangles[tris + 3] = vert + 1;
                triangles[tris + 4] = vert + resolution + 1;
                triangles[tris + 5] = vert + resolution + 2;

                vert++;
                tris += 6;
            }
            vert++;
        }
    }
    
    void UpdateMesh()
    {
        mesh.Clear();
        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.RecalculateNormals();
        mesh.uv = uvs;
    }

    private void UpdateCollider()
    {
        GetComponent<MeshCollider>().sharedMesh = mesh;
    }

    private void Awake()
    {
        mesh = new Mesh();
        GetComponent<MeshFilter>().mesh = mesh;
        Generate();
    }

    private void FixedUpdate()
    {
        if (updateEveryFrame)
        {
            Generate();
        }
    }
    [Button("Generate")]
    private void Generate()
    {
        CreateShapes();
        UpdateUVs();
        UpdateMesh();
        UpdateCollider();
    }

    private void UpdateUVs()
    {
        uvs = new Vector2[vertices.Length];

        for (int i = 0, z = 0; z <= resolution; z++)
        {
            for (int x = 0; x <= resolution; x++)
            {
                uvs[i] = new Vector2((float)x / resolution, (float)z / resolution);
                i++;
            }
        }
    }
}
