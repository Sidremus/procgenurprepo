﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform target;
    Vector3 offset;
    private void Awake()
    {
        offset = target.position - transform.position;
    }

    void Update()
    {
        transform.LookAt(target);
        transform.position = target.position - offset;
        //transform.position = new Vector3(transform.position.x, -offset.y, transform.position.z);
    }
}
