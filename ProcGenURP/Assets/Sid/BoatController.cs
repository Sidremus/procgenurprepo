﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class BoatController : MonoBehaviour
{
    public float pushForce = 300f, timeTillMaxSpeed = 3f, rotForce = 300f;
    public Transform centerOfMass;

    WaterChecker[] waterCheckers;
    Rigidbody rb;
    Keyboard kb;
    bool waterContact;
    float push, rot, pushLerpFac;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        rb.centerOfMass = centerOfMass.position;
        kb = Keyboard.current;
        waterCheckers = GetComponentsInChildren<WaterChecker>();
        if (waterCheckers.Length == 0) Debug.LogWarning("No WaterCheckers found.");
    }
    private void ApplyControls()
    {
        if (waterContact && kb.leftShiftKey.isPressed)
        {
            if (push != 0) rb.AddForce(rb.transform.forward * push * 3f);

            if (rot != 0) rb.AddRelativeTorque(new Vector3(0, rot, 0));
        }
        if (waterContact)
        {
            if (push != 0) rb.AddForce(rb.transform.forward * push);

            if (rot != 0) rb.AddRelativeTorque(new Vector3(0, rot, 0));
        }
        //else if (kb.leftShiftKey.isPressed)
        //{
        //    if (push > 0) rb.AddRelativeTorque(new Vector3(airControl, 0, 0));
        //    else if (push < 0) rb.AddRelativeTorque(new Vector3(-airControl, 0, 0));

        //    if (rot > 0) rb.AddRelativeTorque(new Vector3(0, 0, -airControl));
        //    else if (rot < 0) rb.AddRelativeTorque(new Vector3(0, 0, airControl));
        //}
    }

    private bool CheckWaterContact()
    {
        bool b = true;
        foreach (WaterChecker waterChecker in waterCheckers)
        {
            if (b) b = Physics.Raycast(waterChecker.transform.position, Vector3.down, 500f, 1 << LayerMask.NameToLayer("Water")); 
        }
        return !b;
    }

    private void ReadControls()
    {
        push = 0; rot = 0;

        if (kb.wKey.isPressed)
        {
            pushLerpFac = Mathf.Clamp(pushLerpFac + Time.deltaTime / timeTillMaxSpeed, 0, 1);
            push = Mathf.SmoothStep(0,  pushForce, pushLerpFac);
        }
        else if (kb.sKey.isPressed) 
        {
            pushLerpFac = 0;
            push = -pushForce / 4f; 
        }
        else pushLerpFac = Mathf.Clamp(pushLerpFac - Time.deltaTime / timeTillMaxSpeed, 0, 1);

        if (kb.aKey.isPressed && !kb.dKey.isPressed) rot = -rotForce;
        else if (kb.dKey.isPressed && !kb.aKey.isPressed) rot = rotForce;
    }
    void Update()
    {
        ReadControls();
    }

    private void FixedUpdate()
    {
        waterContact = CheckWaterContact();
        if (waterContact) { rb.drag = 1f; rb.angularDrag = 40f; }
        else { rb.drag = .3f; rb.angularDrag = 5f; }
        ApplyControls();
    }
}
