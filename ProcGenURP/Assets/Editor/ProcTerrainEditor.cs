﻿using UnityEngine;
using UnityEditor;
using DG.Tweening;

[CustomEditor(typeof(ProcTerrain))]
public class ProcTerrainEditor : Editor
{
    int xSize, zSize;

    SerializedProperty perlinZoom;
    float perlinScale;

    float offsetX, offsetY;

    bool autoUpdate = true;


    protected virtual void OnEnable()
    {
        perlinZoom = serializedObject.FindProperty(nameof(perlinZoom));

    }

    public override void OnInspectorGUI()
    {
        ProcTerrain procTerrain = (ProcTerrain) target;
        
        this.serializedObject.Update ();
        



    EditorGUI.BeginChangeCheck();

        GUILayout.Label("Size", EditorStyles.boldLabel);
        xSize = EditorGUILayout.IntField("X size", xSize);
        zSize = EditorGUILayout.IntField("Z size", zSize);

        GUILayout.Space(5);

        GUILayout.Label("Offsets", EditorStyles.boldLabel);
        GUILayout.BeginHorizontal();
        offsetX = EditorGUILayout.FloatField("X axis", offsetX);
        offsetY = EditorGUILayout.FloatField("Y axis", offsetY);
        GUILayout.EndHorizontal();

        GUILayout.Space(5);

        EditorGUILayout.PropertyField(perlinZoom, new GUIContent("Perlin zoom"), GUILayout.Height(20));
        
        //perlinZoom = EditorGUILayout.Slider("Perlin zoom",perlinZoom, .001f,.8f);
        perlinScale = EditorGUILayout.Slider("Perlin scale", perlinScale,.0001f, 50);

        GUILayout.Space(5);

        GUILayout.Toggle(autoUpdate, "Update in Edit Mode : ");
        GUILayout.Space(10);



       

        procTerrain.xSize = xSize;
        procTerrain.zSize = zSize;


        //procTerrain.perlinZoom = perlinZoom;
        procTerrain.perlinScale = perlinScale;

        procTerrain.offsetX = offsetX;
        procTerrain.offsetY = offsetY;

        procTerrain.autoUpdate = autoUpdate;

        


        GUILayout.BeginHorizontal();

        if (GUILayout.Button(("Start Tween")))
        {
            procTerrain.InterpolateTerrain();
        }

        if (GUILayout.Button(("Stop Tween")))
        {
            procTerrain.terrainInterpolation.Kill();
        }

        GUILayout.EndHorizontal();

        if (GUILayout.Button("Force generation"))
        {
            procTerrain.CreateShapes();
        }
        
        if (EditorGUI.EndChangeCheck())
        {
            procTerrain.CreateShapes();
        }
        this.serializedObject.ApplyModifiedProperties ();

    }
}