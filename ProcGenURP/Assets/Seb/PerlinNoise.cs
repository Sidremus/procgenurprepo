﻿using NaughtyAttributes;
using UnityEngine;
using Random = UnityEngine.Random;

public class PerlinNoise : MonoBehaviour
{
    public int width = 256, height = 256;
    public float scale = 20;
    public float offsetX = 300, offsetY = 300;

    [Button()]
    public void Regenerate()
    {
        Renderer rend = GetComponent<Renderer>();
        rend.sharedMaterial.mainTexture = GenerateTexture2D();
    }

    [Button()]
    public void RandomizeTexture()
    {
        offsetX = Random.Range(0, 9999);
        offsetY = Random.Range(0, 9999);
    }

    Texture2D GenerateTexture2D()
    {
        Texture2D texture = new Texture2D(width, height);

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                Color color = ComputeColor(x, y);
                texture.SetPixel(x, y, color);
            }
        }

        texture.Apply();
        return texture;
    }

    Color ComputeColor(int x, int y)
    {
        float xCoord = (float) x / width * scale + offsetX;
        float yCoord = (float) y / height * scale + offsetY;

        float sample = Mathf.PerlinNoise(xCoord, yCoord);

        Color color = new Color(sample, sample, sample);
        return color;
    }

    private void Update()
    {
        GenerateAndRandomize();
    }

    [Button()]
    void GenerateAndRandomize()
    {
        Regenerate();
        RandomizeTexture();
    }
}