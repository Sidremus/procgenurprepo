﻿using System;
using NaughtyAttributes;
using UnityEngine;
using Random = UnityEngine.Random;
using DG.Tweening;

public class ProcTerrain : MonoBehaviour
{
    private Mesh mesh;

    Vector3[] vertices;
    int[] triangles;
    private Vector2[] uvs;

    [HideInInspector] public int xSize = 10, zSize = 10;
    [HideInInspector] [Range(0, 1)] public float perlinZoom;
    [HideInInspector] [Range(0, 10)] public float perlinScale;

    [HideInInspector] [Range(0, 300)] public float offsetX, offsetY;
    public bool autoUpdate = true;


    private void Update()
    {
        CreateShapes();
        UpdateMesh();
        UpdateCollider();
    }


    [Button("Update the colliders yo")]
    private void UpdateCollider()
    {
        GetComponent<MeshCollider>().sharedMesh = mesh;
    }

    public void CreateShapes()
    {
        mesh = new Mesh();
        GetComponent<MeshFilter>().mesh = mesh;

        vertices = new Vector3[(xSize + 1) * (zSize + 1)];

        for (int i = 0, z = 0; z <= zSize; z++)
        {
            for (int x = 0; x <= xSize; x++)
            {
                float y = Mathf.PerlinNoise(x * perlinZoom + offsetX, z * perlinZoom + offsetY) * perlinScale;
                vertices[i] = new Vector3(x, y, z);
                i++;
            }
        }

        int vert = 0, tris = 0;
        triangles = new int[xSize * zSize * 6];

        for (int z = 0; z < zSize; z++)
        {
            for (int x = 0; x < xSize; x++)
            {
                triangles[tris + 0] = vert + 0;
                triangles[tris + 1] = vert + xSize + 1;
                triangles[tris + 2] = vert + 1;
                triangles[tris + 3] = vert + 1;
                triangles[tris + 4] = vert + xSize + 1;
                triangles[tris + 5] = vert + xSize + 2;

                vert++;
                tris += 6;
            }

            vert++;
        }

        uvs = new Vector2[vertices.Length];


        for (int i = 0, z = 0; z <= zSize; z++)
        {
            for (int x = 0; x <= xSize; x++)
            {
                uvs[i] = new Vector2((float) x / xSize, (float) z / zSize);
                i++;
            }
        }

        UpdateMesh();
        UpdateCollider();
    }

    void UpdateMesh()
    {
        mesh.Clear();
        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.RecalculateNormals();
        mesh.uv = uvs;
    }

    public Tween terrainInterpolation;

    public void InterpolateTerrain()
    {
        terrainInterpolation = DOTween.To(() => perlinZoom, x => perlinZoom = x, 1, 5).From(0, true, true)
            .SetLoops(-1, LoopType.Yoyo)
            .SetEase(Ease.Linear);
    }
}