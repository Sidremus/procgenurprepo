﻿using NaughtyAttributes;
using UnityEngine;

public class BallGenerator : MonoBehaviour
{
    
    [Button()]
    public void MoreBalls()
    {
        GameObject go = Instantiate(GameObject.CreatePrimitive(PrimitiveType.Sphere), transform.position,
            Quaternion.identity);
        go.GetComponent<Renderer>().material.color = Random.ColorHSV();
        go.AddComponent<Rigidbody>();
        go.GetComponent<Rigidbody>().mass = Random.Range(2, 20000);
    }
    
}
